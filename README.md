# SPEKULOOM
Develpment of computer intelligence to evaluate language writing.

    Author: Carlo E. T. Oliveira, Cibele R. C. Oliveira
    Affiliation: Universide Federal do Rio de Janeiro
    Email: carlo at nce ufrj br
    
## Neurocomp Section


Neurocomp investigation searchs to extract meaning from text using computational intelligence.
It investigates content semantics studying graphs extracted from texts.


This module downloads a collection of pdf files from SBIE conference. Pdfs are converted to plain text by calling system utilities. Text are then cleaned from publishing artifacts like page headings and numbers.

The text to graph module loads a text file and clean stop words for semantic inferences.
The words are parsed through a word similarity calculator to generate a tag list of semantic relevance for the text contents.

Key words are scanned througout a text within a contextual range. They are related according to the matching count of their intersections.

## Laboratório de Automação de Sistemas Educacionais
Copyright © Carlo Olivera

[LABASE](http://labase.sefip.org) - 
[Instituto Tércio Pacitti](http://nce.ufrj.br) - 
[UFRJ](http://www.ufrj.br)

![LABASE Laboratório de Automação de sistemas Educacionais][logo]

[logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"

