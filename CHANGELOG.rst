Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.

This project adheres to `Semantic Versioning`_.


`Unreleased`_
-------------

Added
+++++
- isomorph graph investigation module.


`0.2.0`_ - (2020-03-05)
-----------------------

Fixed
+++++
- indexer and utilities CLI execution.

Added
+++++
- essentials node calculation module.
- jupyter notebook investigation.

`0.1.0`_ - (2020-03-03)
-----------------------

Enhanced
++++++++
* Improved module documentation, using inner changelogs.
* Sphinx documentation

Added
+++++
- Changelog.md file.
- neurocomp module.
- jupyter notebook investigation.

Changed
+++++++
- Move neurocomp from spike to spekuloom project.
- Explanation in README.md.
- Moved all data to data directory.
- All code to src directory.

Removed
+++++++
- Data files from repository

`0.0.1`_ - (2020-02-29)
-----------------------

Added
+++++
- Sphinx documentation.
- CLI option manager to utilities.

Changed
+++++++
- Mediator uses utility main for CLI parse and run.

-------

Laboratório de Automação de sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**


.. ..

 [LABASE](http://labase.sefip.org) -
 [Instituto Tércio Pacitti](http://nce.ufrj.br) -
 [UFRJ](http://www.ufrj.br)

 ![LABASE Laboratório de Automação de sistemas Educacionais][logo]

 [logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"
 <!---

.. ..

LABASE_ - `Instituto Tércio Pacitti`_ - UFRJ_

.. image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :alt: Laboratório de Automação de sistemas Educacionais




.. _LABASE: http://www.python.org/
.. _Instituto Tércio Pacitti: http://nce.ufrj.org/
.. _ufrj: http://www.UFRJ.BR/
.. _Unreleased: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _0.2.0: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _0.1.0: https://gitlab.com/cetoli/spekuloom/-/commit/aee4f8d0ddb0bd02aa439ba5ac18f285cc7283e7
.. _0.0.1: https://gitlab.com/cetoli/spekuloom/-/commit/157d50c8981d725a08a3cd7a89d84910b31f7fb0
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html

.. ..

 --->

.. ..