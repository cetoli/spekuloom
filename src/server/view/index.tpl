<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Spekuloom
# Copyright 2014-2017 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://j.mp/GNU_GPL3>`__.
#
# Spekuloom é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""Handle http requests.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

"""

-->
<html lang="en">
<head>
    <title>Spekuloom</title>
    <!-- stylesheets -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bulma.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon"/>
</head>
<body>
<!-- navigation -->
<nav class="navbar is-transparent">
  <div class="navbar-brand">
    <a class="navbar-item" href="http://www.superpython.net">
      <img src="/image/camisasuperpython.png" alt="Spekuloom" width="60" height="28">
    </a>
    <div id="burg_menu" class="navbar-burger burger" data-target="navbarExampleTransparentExample">
            <a class="navbar-item is-tab" href="#">

            </a>
            <a class="navbar-item is-tab" href="/site/help.html">
                Help
            </a>
            <a class="navbar-item is-tab" href="/site/about.html">
                About
            </a>
            <a class="navbar-item is-tab" href="/">
                Home
            </a>
    </div>
  </div>

  <div id="navbarExampleTransparentExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="/">
        Spekuloom
      </a>
    </div>

    <!-- this "nav-menu" is hidden on mobile -->
    <div id="right_menu" class="navbar-end">
        <a class="navbar-item is-tab" href="/site/help.html">
            Help
        </a>
        <a class="navbar-item is-tab" href="/site/about.html">
            About
        </a>
    </div>
    <!-- end of nav -->
  </div>
</nav>
<!-- end navigation -->

<!-- page header (title, etc) -->
<div class="main-header">
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="has-text-centered">
                    <!-- header && subheader -->
                    <h1 class="title is-1 is-spaced">Spekuloom</h1>
                    <h4 class="subtitle is-4">learn language with metacognition</h4>
                    <!-- end of header && subheader -->

                </div>
            </div>
        </div>
    </section>
</div>
<!-- end page header -->

<!-- page content -->
<div class="main-content">
    <div class="container">
        <!-- start of posts -->
        <div class="columns is-multiline is-centered has-text-centered">
            <!-- start of post -->
            <div class="column is-4">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <textarea rows="15" cols='30'  class="is-4by3">
                        </textarea>
                    </div>
                    <!-- end of image for post -->

                    <!-- post header -->
                    <div class="card-content-header">
                        <h4 class="title is-4"><a href="/supygirls/project">SuperPython</a></h4>
                    </div>
                    <!-- end of post header -->
                    Gather your team to develop your own game

                    <!-- post content -->
                </div>
            </div>
            <!-- end of post -->

            <!-- start of post -->
            <div class="column is-4">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <textarea rows="15" cols='30' class="is-4by3">
                        </textarea>
                    </div>
                    <!-- end of image for post -->

                    <!-- post header -->
                    <div class="card-content-header">
                        <h4 class="title is-4"><a href="/kwarwp">Kwarwp</a></h4>
                    </div>
                    <!-- end of post header -->
                    Follow the adventures of a brave warrior
                    <!-- post content -->
                </div>
            </div>
            <!-- end of post -->

            <!-- start of post -->
            <div class="column is-4">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <a href="/supygirls/project">
                                <img src="image/supygirls_logo.png" alt="Image">
                            </a>
                        </figure>
                    </div>
                    <!-- end of image for post -->

                    <!-- post header -->
                    <div class="card-content-header">
                        <h4 class="title is-4"><a href="/supygirls/project">SuPyGirls</a></h4>
                    </div>
                    <!-- end of post header -->
                    Roll your own game with python
                    <!-- post content -->
                </div>
            </div>
            <!-- end of post -->
        </div>
        <!-- end of posts -->
    </div>

</div>
<!-- end of page content -->

<!-- footer: will stick to the bottom -->
<div class="footer footer-top-shadow">
    <div class="container">
        <!-- start of posts -->
        <div class="columns is-centered">
            <!-- start of post -->
            <div class="column is-2">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <figure class="image is-4by1">
                            <a href="https://ufrj.br/">
                                <img src="image/ufrj-logo-8.png"  alt="UFRJ">
                            </a>
                        </figure>
                    </div>
                    <!-- end of image for post -->
                </div>
            </div>
            <!-- end of post -->
            <!-- start of post -->
            <div class="column is-2">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <figure class="image is-4by1">
                            <a href="http://www.nce.ufrj.br/">
                                <img src="image/nce-logo-8.png" height="30px" alt="NCE">
                            </a>
                        </figure>
                    </div>
                    <!-- end of image for post -->
                </div>
            </div>
            <!-- end of post -->
            <!-- start of post -->
            <div class="column is-2">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <figure class="image is-4by1">
                            <a href="http://labase.superpython.net/">
                                <img src="image/labase-logo-8.png" height="30px" alt="LABASE">
                            </a>
                        </figure>
                    </div>
                    <!-- end of image for post -->
                </div>
            </div>
            <!-- end of post -->
            <!-- start of post -->
            <div class="column is-2">
                <div class="card">
                    <!-- image for post -->
                    <div class="card-image">
                        <figure class="image is-3by1">
                            <a href="http://www.sbc.org.br/2-uncategorised/1939-programa-superpython">
                                <img src="image/sbc-logo-8.png" alt="SBC">
                            </a>
                        </figure>
                    </div>
                    <!-- end of image for post -->
                </div>
            </div>
            <!-- end of post -->
        </div>
        <!-- end of posts -->
    </div>
    <div class="container has-text-centered">


        <br>
        <p>gaming platform by <a href="http://www.superpython.net">www.superpython.net</a></p>
        <p>this platform is proudly open source</p>
    </div>
</div>
<!-- end of footer -->
</body>
</html>