#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Vitollino
# Copyright 2014-2017 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://j.mp/GNU_GPL3>`__.
#
# Vitollino é um software livre; você pode redistribuí-lo e/ou
# modificá-lo dentro dos termos da Licença Pública Geral GNU como
# publicada pela Fundação do Software Livre (FSF); na versão 2 da
# Licença.
#
# Este programa é distribuído na esperança de que possa ser útil,
# mas SEM NENHUMA GARANTIA; sem uma garantia implícita de ADEQUAÇÃO
# a qualquer MERCADO ou APLICAÇÃO EM PARTICULAR. Veja a
# Licença Pública Geral GNU para maiores detalhes.
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em <http://www.gnu.org/licenses/>

"""Main controller with bottle.

.. moduleauthor:: Carlo Oliveira <carlo@nce.ufrj.br>

"""
from bottle import default_app, view, get, run, static_file  # , post, request, static_file
import logging as log
import os
project_server = os.path.dirname(os.path.abspath(__file__))
css_dir = os.path.join(project_server, '../view/css')
LAST = 1


@get('/')
@view('index')
def register_screen():
    """Return a peer id to identify the user.

    :return: page with Brython client.
    """
    global LAST
    # LAST += 1
    gid = "-".join(["P_N_O_D_E", "%02d" % LAST])
    # print('game register', gid)
    log.debug('game register %s', gid)
    return dict(lastid=0)


# Static Routes
@get("/css/<filepath:path>")
def view_css(filepath):
    return static_file(filepath, root=css_dir)


def main(direc=""):
    """Run Bottle Server

    :return: None.
    """
    global SRC_DIR
    SRC_DIR = direc
    run(host='localhost', port=8080)


application = default_app()

if __name__ == "__main__":
    main()
