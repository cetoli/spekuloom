#! /usr/bin/env python3
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Investigate relevant edges and nodes for the essential structure of graphs.

The GraphText class read a json file describing a graph.
The form is::

    {"nodes": [{"id": "<>", "group": "<>", "value": i}*n],
    [{"source": "<>", "target": "<>", "label": "<>", "value": j}*m]}

It find the relevant node and edge names to form areduced graph of essential nodes and edges.

Example CLI:
    Find essentials in graph description file (will generate original0.json)::

        $ python3.8 essentials.py -f -i original5.json

Classes in this module:

    :py:class:`GraphEssentials` Creates a essentials graph from a graph inspecting word frequencies.

Changelog
---------
    *Upcoming* (2020-03-xx)
        * TODO: Generate a class for CLI parsing.
        * BETTER: Split graph processing from visualisation.
    *0.2.0* (2020-03-05)
        * ADD: GraphEssentials class.

.. seealso::

   Page :ref:`core_introduction`

"""

from utilities import main, helper
import pandas as pd


class GraphEssentials:
    """Investigate relevant edges and nodes for the essential structure of graphs.

    .. seealso::

       Page :ref:`core_essentials`

    """

    def __init__(self):
        self.data_node_nodes = None
        self.data_node_links = None

    def load_graph_from_file(self, input_graph):
        """Loads graph definition in json format.

        :py:class:`GraphEssentials`

        :param input_graph: The graph input file.
        """
        def linker(source, target, label, width):
            return dict(source=source, target=target, label=label, value=int(width))
        from json import load
        graph_descriptor = input_graph
        with open(graph_descriptor, "r") as gd:
            graph = load(gd)
            nodes = graph["nodes"]
            links = graph["links"]
            nodes = [(lambda id, group, val: dict(id=id, group=group, value=int(val)))(**rec) for rec in nodes]

            links = [linker(**rec) for rec in links]
            self.data_node_nodes = pd.DataFrame(list(nodes), columns=['id', 'group', 'value'])
            self.data_node_links = pd.DataFrame(list(links), columns=['source', 'target', 'label', 'value'])

    def filter_by_most_relevant_nodes(self, input_graph, output_graph=None):
        """Scan graph for relevant nodes and edges to form a more concise graph.

        :py:class:`GraphEssentials`

        :param input_graph: The graph input file.
        :param output_graph:  The graph output file, if omited add a "0" to input name.
        """
        output_graph = output_graph or input_graph.replace(".json","0.json")
        self.load_graph_from_file(input_graph)
        data_link_, data_node_ = self.data_node_links, self.data_node_nodes
        tag_word_count = self.data_node_links["source"].value_counts()
        data_link_s = pd.DataFrame({'node': tag_word_count.index, 'scount': tag_word_count.values})
        tag_word_count = self.data_node_links["target"].value_counts()
        data_link_t = pd.DataFrame({'node': tag_word_count.index, 'tcount': tag_word_count.values})
        tag_word_count = self.data_node_links["label"].value_counts()
        data_link_k = pd.DataFrame({'node': tag_word_count.index, 'lcount': tag_word_count.values})
        data_link_n = pd.merge(data_link_s, data_link_t, left_index=True, right_index=True).head(20)
        data_link_nst = pd.merge(data_link_n, data_link_k, left_index=True, right_index=True).head(20)
        essentials = " ".join(set(data_link_nst.node_x.values).intersection(data_link_nst.node_y.values))
        data_link_cg = data_link_.loc[data_link_['target'].isin(essentials.split()) |
                                      data_link_['source'].isin(essentials.split()) |
                                      data_link_['label'].isin(essentials.split())]
        nodenames = set(data_link_cg.source.values).union(data_link_cg.target.values, data_link_cg.label.values)
        data_node_essential_graph = data_node_.loc[data_node_['id'].isin(nodenames)]
        dict_essential_graph = dict(
            nodes=data_node_essential_graph.to_dict('records'), links=data_link_cg.to_dict('records'))
        from json import dump
        graph_descriptor = output_graph
        with open(graph_descriptor, "w") as gd:
            dump(dict_essential_graph, gd)

    def main(self, i=None, o=None, **kwargs):
        """Mediator method execute the filter method collecting arguments for the file names.

        :py:class:`GraphEssentials`

        :param i: Receive argument for input graph file.
        :param o: Receive argument for output graph file.
        :param kwargs: recognise *-f* as the option selecting the filter method.
        """
        arguments = dict(
            input_graph=i,
            output_graph=o
        )
        runner = dict(
            f=self.filter_by_most_relevant_nodes
        )
        [runner[op](**arguments) for op, arg in kwargs.items() if arg]
    options = dict(
        i=helper(",in,input graph (.json)"),
        o=helper(",out,output graph (.json)"),
        f=helper(",,filter relevant nodes and edges"),
    )
    """Options used to configure the main cli option parser."""


if __name__ == "__main__":
    GraphEssentials().main(**main("indexer.py", option_string=GraphEssentials.options))
