#! /usr/bin/env python3
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Index the words in a text file forming a graph, pruning stop words and similarities

The GraphText class read a text file and clean stop words for semantic inferences.
The words are parsed through a word similarity calculator to generate a tag list of
semantic relevance for the text contents.

Example CLI:
    Prune duplicate words from file::

        $ python3.8 indexer.py -p -w stop_word.txt

    Generate 3D graph::

        $ python3.8 indexer.py -f -g graph.json -r relation.json -e edges.txt

Classes in this module:

    :py:class:`File` Default file names for methods and other default constants.

    :py:class:`GraphText` Creates a graph from a text inspecting word frequencies.

    :py:class:`Mediator` Runs given predefined scrits to trgger correct execution of GraphText.

Changelog
---------
    *Upcoming* (2020-03-xx)
        * TODO: Move Mediator to utilities.
    0.2.0 (2020-03-05)
        * FIX: Provide config of parametric files for CLI.
    0.1.0 (2020-03-01)
        * BETTER: Document classes and methods.
    0.0.1 (2020-02-29)
        * NEW: Sphinx Documentation.
        * BETTER: Mediator now uses utility main for CLI.

.. seealso::

   Page :ref:`core_introduction`

"""
from collections import Counter
from graphviz import Graph
from json import dump, load
from utilities import similars, make_format, helper, main


class File:
    """Default file names for methods and other default constants.

        :py:class:`GraphText`

    .. seealso::

       Page :ref:`core_indexer`

    """
    conf = "../../data/conf/{}"
    """diretório de configurações"""
    data = "../../build/{}"
    """diretório de saída de arquivos gerados"""
    corp = "../../data/corpora/{}"
    """diretório de arquivos de entrada"""
    texto_original = conf.format("textomscindex.txt")
    stop_words = conf.format("stopwords.txt")
    non_stop = conf.format("nonstopnn.txt")
    non_stop_cloud = conf.format("nonstopcloud.txt")
    similar_words = conf.format("similar_word_list_bad.txt")
    mscivgraph = data.format("mscivgraph.json")
    word_cloud = data.format("nonstopcloud.txt")
    relation_file = conf.format("word_relations.json")
    edge_file = data.format("links.txt")
    search_range = 7
    options = dict(
        a=helper(",parm,amplitude of scan range span, default 7"),
        s=helper(",inou,word similarity file [meaning alias*]"),
        e=helper(",out,edge labeling file [s -> t : l]"),
        g=helper(",out,graph file to 3D forge viewer"),
        r=helper(",in,word relation dictionary json {s: t: l:}"),
        t=helper(",in,text containing the full file contents"),
        n=helper(",in,text containing repeated words"),
        w=helper(",inou,stop words list not relevant to meaning"),
        p=helper(",,prune stop words: remove duplicates <-p -w stop_word.txt>"),
        d=helper(",,create similarity dictionary <-p -s stop_word.txt>"),
        c=helper(",,create a repeated list of words <-c -t text.txt -n newtext.txt>"),
        f=helper(",,generate 3D graph <-f -t text.txt -g graph.json -r relation.json -e edges.txt>")
    )


class GraphText:
    """Creates a graph from a text inspecting word frequencies.

    :param text: A string of text containing the full file contents.
    :param textfile:  Name of file to load the full file contents.
    :param similarities: Name of file to load a list with similar words.
    :param non_stop: do not process stop words if True

    Methods in this class:

        :py:meth:`remove_stop_words`

        :py:meth:`create_taglist_lowercase`

        :py:meth:`create_taglist_lowercase_from_file`

        :py:meth:`create_taglist_lowercase_numbered`

        :py:meth:`create_equivalence_dict`

        :py:meth:`initiate_graph`

        :py:meth:`initiate_3dforce`

    .. seealso::

       Page :ref:`core_indexer`

    """

    def __init__(self, text: str = None, textfile: str = File.texto_original, similarities=None, non_stop=None):

        self.text = text.split()[1:] if text else self.load(textfile)
        self.taglist = {}
        self.remove_stop_words()
        self.create_taglist_lowercase_from_file(non_stop) if non_stop else self.create_taglist_lowercase()
        self.similarities = {}
        self.create_text_from_given_taglist(similarities) if similarities else None

    @staticmethod
    def load(textfile):
        """Load the full text from file.

        :param textfile: Name of file to load the full file contents.
        :return: a list of the words from the file.
        """
        with open(textfile, "r") as tags:
            print("textfile", textfile)
            return tags.read().split()[1:]

    def remove_stop_words(self, stop_file=File.stop_words):
        """Remove unwanted word from the text list.

        :py:class:`GraphText`

        :param stop_file: Name of file to load a lidt with stop words.
        :return: the pruned text list.
        """
        with open(stop_file, "r") as stop_tags:
            stop_words = stop_tags.read().split()
            print(f"stop_words{stop_words}")
            self.text = [tag.lower().strip(".!?;-+(),@#$%&*{}ªº") for tag in self.text]
            self.text = [word for word in self.text if (word not in stop_words) and len(word) > 2]

    def create_taglist_lowercase(self):
        """Create a dictionary with the count of each word in the text.

        :py:class:`GraphText`

        """
        taglist = [tag for tag in self.text if tag[:-1] not in self.text]
        self.taglist = Counter(taglist)

    def create_text_from_given_taglist(self, similarities=File.similar_words):
        """Load the full text from file and remove similar words from the text list.

        :py:class:`GraphText`

        :param similarities: Name of file to load a lidt with stop words.
        """
        with open(similarities, "r") as similars_list:
            taglist = [
                record.split() for record in similars_list.read().split("\n")
                if (len(record) > 3 and (len(record.split()) > 1))]
            taglist = {word: similar for word, *similar in taglist}
            taglist = {simile: word for word, similar in taglist.items() for simile in similar}
            self.text = [tag if tag not in taglist else taglist[tag] for tag in self.text]

    def create_taglist_lowercase_from_file(self, non_stop_file=File.non_stop):
        """Load the relevant tags from file ignoring blank records.

        :py:class:`GraphText`

        :param non_stop_file: Name of file to load a list of relevant tag words.
        """
        with open(non_stop_file, "r") as nsfile:
            taglist = [record.split()[0] for record in nsfile.read()[1:].split("\n") if len(record) > 3]
            # taglist = [record for record in nsfile.read()[1:].split("\n") if len(record) > 2]
            self.taglist = {word: 0 for word in taglist}
            print(f"self.taglist: {self.taglist}")
            self.text = [tag for tag in self.text if tag in self.taglist]

    def create_taglist_lowercase_numbered(self, non_stop_file=File.non_stop):
        """Load the relevant tags from file ignoring blank records and other information.

        :py:class:`GraphText`

        :param non_stop_file: Name of file to load a list of relevant tag words.
        """
        with open(non_stop_file, "r") as nsfile:
            taglist = [record.split() for record in nsfile.read()[1:].split("\n") if len(record) > 3]
            self.taglist = {word: int(count) for word, count in taglist}
            print(f"self.taglist: {self.taglist}")
            self.text = [tag for tag in self.text if tag in self.taglist]

    def _create_equivalence_dict(self, equivalence_file=File.similar_words):
        with open(equivalence_file, "w") as eq_file:
            self.similarities = similarities = {
                word_key: [similars(word_key, sample_word)
                           for sample_word in self.taglist if similars(word_key, sample_word)]
                for word_key in self.taglist}
            [eq_file.write(make_format(key_word, eq_list)) for key_word, eq_list in similarities.items()]

    def create_equivalence_dict(self, equivalence_file=File.similar_words):
        """Evaluate in the text similar words an write the list to a file.

        :py:class:`GraphText`

        :param equivalence_file: Name of file to write a list with similar words.
        """
        with open(equivalence_file, "w") as eq_file:
            taglist = self.taglist.keys()
            self.similarities = similarities = {word_key: [] for word_key in taglist}
            for word_key in taglist:
                for sample_word in taglist:
                    similarity = similars(word_key, sample_word)
                    if similarity:
                        similarities[word_key].append(similarity)
            similarities = {key: value for key, value in similarities.items() if value}
            [eq_file.write(make_format(key_word, eq_list)) for key_word, eq_list in similarities.items()]

    def initiate_graph(self, filename=f'data/msciv.gv', dt=7):
        """Evaluate ithe text to generate a 2D prot of a relations graph.

        :py:class:`GraphText`

        :param filename: Name of file to write a description of the graph.
        :param dt: Word span to investigate context relations.
        """
        filename = filename.replace('.', f"{dt}.")
        taglist = self.text
        taggraph = Counter()
        # dt = 7
        dot = Graph(comment='Mestrado Indústria 4.0', format="png", engine="dot", strict=True)
        [taggraph.update({(a, b): dt - i for a, b in zip(taglist, taglist[i:]) if a != b}) for i in range(1, dt + 1)]
        # tagback = {(b, a): count for (a, b), count in taggraph.items() if (b, a) in taggraph}
        # taggraph.update({(a, b): count for (b, a), count in tagback.items()})
        # [taggraph.pop(key) for key in tagback if key in taggraph]
        # taggraph = {a: b for a, b in taggraph.items() if b > dt*1.75}
        taggraph = {a: b for a, b in taggraph.items() if b > dt}
        print(taggraph)
        edgeweight = set(i // 5 / 2 for i in taggraph.values())
        tagcnt = Counter(a for a, _ in taggraph.keys())
        tagcnt.update(a for _, a in taggraph.keys())
        tagcntset = sorted(list(set(tagcnt.values())), reverse=True)
        tagdict = {n: f"n{i}" for i, n in enumerate(tagcnt.keys())}
        color = "red pink orange green blue cyan purple gray".split()
        colordict = {name: color[min(tagcntset.index(value), len(color) - 1)] for name, value in tagcnt.items()}
        # fontdict = {name: font[min(tagcntset.index(value), len(color)-1)] for name, value in tagcnt.items()}
        # font = list(range(48, 2, -2))
        # fl = len(font)
        nodeweight = list(set(i // 8 for i in tagcnt.values()))[::-1]
        nodeweight = {size: col for size, col in zip(nodeweight, color)}
        nodeweight[0] = "gray"
        nodesize = {name: (count // 8 + 1) for name, count in tagcnt.items()}
        nodefontsize = {name: (count // 2 + 12) * 2.0 for name, count in tagcnt.items()}
        nodefontsize[0] = 4.0
        edgelenghtmax = elx = max(set(taggraph.values())) + 1
        [dot.node(
            i, n, color=colordict[n], penwidth=f'{2.0 + nodefontsize[n] / 2}',
            height=f"{nodesize[n] / 2}", width=f"{nodesize[n]}", fontsize=str(nodefontsize[n]),
        ) for n, i in tagdict.items()]
        # [dot.node(
        #     i, n, color=nodeweight[tagcnt[n]//8], penwidth=f'{2.0+ nfs[n]/2}',
        #     height=f"{ns[n]/2}", width=f"{ns[n]}", fontsize=str(nfs[n]),
        # ) for n, i in tagdict.items()]
        [dot.edge(tagdict[a], tagdict[b], penwidth=str(s), len=str((elx - s) / 10)) for (a, b), s in taggraph.items()]
        print(edgeweight, len(taggraph), tagdict, taggraph)
        print(edgelenghtmax, nodefontsize, nodeweight, len(tagcnt), tagcnt)
        dot.render(filename=filename, view=True)

    def initiate_3dforce(self, mscivgraph_file=File.mscivgraph, relation_file=None, link_file=None, dt=7):
        """Evaluate ithe text to generate a 2D prot of a relations graph.

        :py:class:`GraphText`

        :param mscivgraph_file: Name of file to write a description of the graph.
        :param relation_file: Name of file to load a list of labels relating two words.
        :param link_file: Name of file to write a list of labels relating two words.
        :param dt: Word span to investigate context relations.

        Reference Page :ref:`core_indexer`
        """

        def incorporate_relation_labels():
            with open(relation_file, "r") as file_relation:
                return {
                    tuple("{source} {target}".format(**record).split()): "{label}".format(**record)
                    for record in load(file_relation)}

        taglist = self.text
        taggraph = Counter()
        # dt = 7
        relations = incorporate_relation_labels() if relation_file else {}
        # print("relations", relations)
        [taggraph.update({(a, b): dt - i for a, b in zip(taglist, taglist[i:]) if a != b}) for i in range(1, dt + 1)]
        taggraph = {a: (b, relations[a]) if a in relations else (b, "---")
                    for a, b in taggraph.items() if b > dt}
        # taggraph = {a: b for a, b in taggraph.items() if b > dt}
        [print(f"{source} -> {target} : {label}") for (source, target), (_, label) in taggraph.items()]
        tagcnt = Counter(a for a, _ in taggraph.keys())
        tagcnt.update(a for _, a in taggraph.keys())
        tagcntset = sorted(list(set(tagcnt.values())), reverse=True)
        tagdict = {n: f"n{i}" for i, n in enumerate(tagcnt.keys())}
        color = "red pink orange green blue cyan purple gray".split()
        colordict = {name: color[min(tagcntset.index(value), len(color) - 1)] for name, value in tagcnt.items()}
        nodeweight = list(set(i // 8 for i in tagcnt.values()))[::-1]
        nodeweight = {size: col for size, col in zip(nodeweight, color)}
        nodeweight[0] = "gray"
        nodesize = {name: (count // 8 + 1) for name, count in tagcnt.items()}
        mscivgraph = dict(
            nodes=[dict(id=nome, group=colordict[nome], val=nodesize[nome] * 4) for nome, _ in tagdict.items()],
            links=[{"source": a, "target": b, "width": s // 4, "label": l}
                   for (a, b), (s, l) in taggraph.items() if (b, a) not in taggraph],
        )
        with open(mscivgraph_file.replace(".json", f"{dt}.json"), "w") as mscivgr:
            dump(mscivgraph, mscivgr)
        if link_file:
            with open(link_file, "w") as lfile:
                [lfile.write(f"{source} -> {target} : {label}\n") for (source, target), (_, label) in taggraph.items()]


class Mediator:
    """Runs given predefined scrits to trgger correct execution of GraphText.

    :param t: text containing the full file contents.
    :param s: word similarity file, each line with  <meaning alias*>.
    :param r: word relation dictionary in json format.
    :param e: text file with edge labeling specs <s -> t : l>.
    :param g: graph file to 3D forge viewer in json format.
    :param n: text containing repeated words found i a given text.
    :param w: stop words list not relevant to meaning.
    :param kwargs: receive options e, f, p or c

    Option action:

        * e: Create equivalence dictionary.
        * f: Create 3d force graphic json input.
        * p: Prune stop words list.
        * c: Generate cloud.

    .. seealso::

       Page :ref:`core_indexer`

    """

    def __init__(self, x=None, t=None, s=None, r=None, e=None, g=None, n=None, w=None, a=None, **kwargs):
        self.arguments = dict(
            delta_text=a or File.search_range,
            similarities=s or File.similar_words,
            relation_file=r or File.relation_file,
            link_file=e or None,  # File.edge_file,
            graph_file=g or File.mscivgraph,
            stop_file=w or File.stop_words,
            cloud_file=n or File.stop_words,
            non_stop_file=x or File.non_stop_cloud,
            textfile=t or File.texto_original
        )
        runner = dict(
            e=self.create_equivalence_dictionary,
            f=self.create_3d_force_graphic_json_input,
            p=self.prune_stop_words_list,
            c=self.generate_cloud,
        )
        runner[x] if x else [runner[op](**self.arguments) for op, arg in kwargs.items() if arg]

    @staticmethod
    def create_3d_force_graphic_json_input(
         similarities=File.similar_words, relation_file=File.relation_file, delta_text=File.search_range,
            link_file=File.edge_file, textfile=File.texto_original, graph_file=File.mscivgraph, **_
    ):
        print("3dforce", textfile, link_file, graph_file)
        graf_text = GraphText(textfile=textfile, similarities=similarities)
        graf_text.initiate_3dforce(mscivgraph_file=graph_file, dt=int(delta_text), relation_file=relation_file,
                                   link_file=link_file)

    @staticmethod
    def create_equivalence_dictionary(**_):
        graf_text = GraphText()
        graf_text.create_equivalence_dict()

    @staticmethod
    def prune_stop_words_list(stop_file=File.stop_words, **_):
        with open(stop_file, "r") as stop_tags:
            stop_words = sorted(set(stop_tags.read().split()))
            print(f"stop_words{stop_words}")
        with open(stop_file, "w") as stop_tags_write:
            stop_tags_write.write("\n".join(stop_words))

    @staticmethod
    def generate_cloud(non_stop_file: str = File.non_stop, cloud_file: str = File.word_cloud, **_) -> None:
        """Generate a list of reapeated words given its ocurrence count to draw a word cloud.
    
        :param non_stop_file: Name of file to load a list of relevant tag words.
        :param cloud_file: Name of file to write a list of unique relevant tag words alongside their counts.
        """
        with open(non_stop_file, "r") as nsfile:
            in_tags = [record.split() for record in nsfile.read()[1:].split("\n") if len(record) > 3]
            print(in_tags)
            # return
            with open(cloud_file, "w") as eq_file:
                [[eq_file.write(f"{key_word} ") for _ in range(int(count))] for key_word, count, *_ in in_tags]


if __name__ == "__main__":
    Mediator(**main("indexer.py", option_string=File.options))
