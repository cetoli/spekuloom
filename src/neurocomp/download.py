#! /usr/bin/env python
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Download pdfs from url, convert to text and clean.

    This module downloads a collection of pdf files from SBIE conference.
    Pdfs are converted to plain text by calling system utilities.
    Text are then cleaned from publishing artifacts like page headings and numbers.

Example CLI:
    Dowload files from url::

        $ python3.8 download.py


Classes in this module:

    :py:class:`CONST` Default file names for methods and other default constants.

    :py:class:`TextPreProcess` Downloads and convert PDF into plain text..

Changelog
---------
    1.0.1 (2020-03-xx) *Upcoming*
        * TODO: Use a Mediator class for CLI parsing.
    1.0.0 (2020-03-01)
        * BETTER: Document classes and methods.
    0.0.2 (2020-02-29)
        * NEW: Sphinx Documentation.

.. seealso::

   Page :ref:`core_introduction`

"""
import re
import subprocess
from time import sleep
from bs4 import BeautifulSoup
from urllib.request import urlopen, urlretrieve
from json import dump, load
import sys

sys.path.insert(0, 'conf')


class CONST:
    """Constant container for the module, defines defaul and simirarity threshold.

        .. seealso::

           Page :ref:`core_downloader`

    """
    site_url = "https://br-ie.org/pub/index.php/sbie"
    conf = "conf/{}"
    data = "data/{}"
    texto = "text/{}"
    file_link = conf.format("file_link.json")
    header1 = "VIII Congresso Brasileiro de Informática na Educação (CBIE 2019)\n"
    header2 = header1 + "Anais do XXX Simpósio Brasileiro de Informática na Educação (SBIE 2019)\n\n"
    Patterns = [(header2, ""), ("", "")]


class TextPreProcess:
    """ Download and convert files from PDF.

    :param str link: Url link as target to request the files to download.

    .. seealso::

       Page :ref:`core_downloader`

    """

    def __init__(self, link=None):
        self.text_link = link or self.scrap_from_url()

    def scrap_from_url(self, url=CONST.site_url, url_list=CONST.file_link):
        """ Scrap page from url, collecting the links of files to be downloaded.

        :param url: Url of page containing a collections of links to files.
        :param url_list: List of direct download links to collect the files.
        :return: a list of links used to downloaded files.
        :rtype: list

        .. seealso::

           Page :ref:`core_downloader`

        """
        html_page = urlopen(url)
        self.text_link = []
        soup = BeautifulSoup(html_page, features="html5lib")
        self.text_link = [
            link.get('href').replace("view", "download")
            for link in soup.findAll('a', attrs={'href': re.compile("^https://")})
            if "/6" in link.get('href')[-6:]]
        # link.get('href').replace()
        print(f"self.text_link {self.text_link}")
        with open(url_list, "w") as confile:
            dump(self.text_link, confile)
        return self.text_link

    def download_files(self):
        """Download the files from a list of links.

        .. seealso::

           Page :ref:`core_downloader`

        """
        for link in self.text_link[:3]:
            file_name = CONST.texto.format(f"texto{link[-4:]}.pdf")
            print(file_name)
            sleep(1)
            urlretrieve(link, file_name)

    def convert_pdf_to_txt(self):
        """ Call the system utility pdftotext for each downloded file.


        .. seealso::

           Page :ref:`core_downloader`

        """
        for link in self.text_link[:3]:
            file_name = CONST.texto.format(f"texto{link[-4:]}.pdf")
            out_filename = file_name.replace(".pdf", ".txt")
            subprocess.run(["pwd"])
            subprocess.run(["pdftotext", file_name, out_filename])

    def clean_txt_files(self):
        """ remove branding and numbers from the files.

        .. seealso::

           Page :ref:`core_downloader`

        """
        regular = r"\nFigura|Tabela .*\n\n|\[.* \d+\]|\(\d+\)|\n\n\d+\n\n|\nX\n|(\d+\.*)+,*\d+| [+-=%]"
        regfig = re.compile(regular, re.IGNORECASE)
        for link in self.text_link[1:3]:
            file_name = CONST.texto.format(f"texto{link[-4:]}.txt")
            file_out = CONST.texto.format(f"ctexto{link[-4:]}.txt")
            with open(file_name, "r") as text_file:
                text = text_file.read()
                # text = regex.sub("", text)
                text = regfig.sub("", text)
                # [print(line) for line in text.split("\n")[100:300]]
                for i, j in CONST.Patterns:
                    text = text.replace(i, j)
                # [print(line) for line in text.split("\n")[100:300]]
                with open(file_out, "w") as text_out:
                    text_out.write(text)


def main():
    import os.path
    link = None
    if os.path.isfile(CONST.file_link):
        print(f"os.path.isfile {CONST.file_link}")
        with open(CONST.file_link, "r") as confile:
            link = load(confile)
    text_pro = TextPreProcess(link=link)
    current = 1
    print(f"text processed: ", CONST.texto.format(f"ctexto{text_pro.text_link[current][-4:]}.txt"))
    # text_pro.scrap_from_url()
    # text_pro.download_files()
    # text_pro.convert_pdf_to_txt()
    text_pro.clean_txt_files()
    # return
    from indexer import GraphText
    file_name = CONST.texto.format(f"ctexto{text_pro.text_link[current][-4:]}.txt")
    file_out = CONST.data.format(f"ctexto{text_pro.text_link[current][-4:]}.json")
    gtext = GraphText(textfile=file_name)
    gtext.initiate_3dforce(file_out)
    gtext.initiate_graph(file_out[:-4] + "gv")


if __name__ == "__main__":
    main()
