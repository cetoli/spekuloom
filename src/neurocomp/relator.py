#! /usr/bin/env python3
# -*- coding: UTF8 -*-
# Este arquivo é parte do programa Neurocomp
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Calculate similar words, dynamic format string, strip stop words, process cli args.

    Key words are scanned througout a text within a contextual range.
    They are related acconding to the matching count of their intersections.

Example CLI:
    Test the cli parser::

        $ python3.8 relator.py -l


There are no modules attributes, they are packed inside a class not to clutter namespace.

Classes in this module:

    Class :class:`CONST` Constant container for the module, defines default values, diretories and filenames.

    Class :class:`TagRelator` Scan for contextual relations among words to name graph edges.

    Class :class:`Mediator` Runs given predefined scrits to trigger correct execution of TagRelator.

Changelog
---------
    1.0.1 (2020-03-xx) *Upcoming*
        * TODO: Improve performance code streamline and parallelization.
    1.0.0 (2020-03-01)
        * NEW: Class and method documentation.
        * NEW: Mediator class to run from CLI
        * NEW: Options added to const as helper to CLI
    0.0.1 (2020-02-29)
        * NEW: Sphinx Documentation.

.. seealso::

   Page :ref:`core_introduction`

"""
from json import dump
from utilities import similars, no_stops, main, helper

SIMILARITY_THRESHOLD = 0.5


class CONST:
    """Constant container for the module, defines default values, diretories and filenames.

        .. seealso::

           Page :ref:`core_relator`

    """
    num_page = 6
    stop_clipper = 2
    node_size_theshold = 4
    node = "{id}"
    value = "val"
    nodes = "nodes"
    edges = "links"
    source = "source"
    target = "target"
    edge = f"{{{source}}} {{{target}}}"
    conf = "../../data/conf/{}"
    """diretório de configurações"""
    data = "../../data/build{}"
    """diretório de saída de arquivos gerados"""
    file_link = conf.format("mscivgraph5.json")
    link_defs = conf.format("word_definitions_base.json")
    """Json file storing edge labels relating two nodes [{source:<>,target:<>,label<>}*n]"""
    texto_original = conf.format("textomscindex.txt")
    """Text file with the reference contents where relations investigations occur."""
    relation_file = conf.format("word_relations.json")
    stop_words = conf.format("stopwords.txt")
    non_stop = conf.format("nonstopnn.txt")
    non_stop_cloud = conf.format("nonstopcloud.txt")
    # similar_words = conf.format("similar_word_list_bad.txt")
    # TODO: refactor >> mscivgraph = data.format("mscivgraph.json")
    word_cloud = data.format("nonstopcloud.txt")
    # edge_file = data.format("links.txt")
    options = dict(
        s=helper(",inou,word similarity file [meaning alias*]"),
        e=helper(",out,edge labeling file [s -> t : l]"),
        r=helper(",in,word relation dictionary json {s: t: l:}"),
        t=helper(",in,text containing the full file contents"),
        w=helper(",inou,stop words list not relevant to meaning"),
        b=helper(",,build relations from original text database>"),
        l=helper(",,retrieve relations from existing link base>"),
    )
    """ b=self.build_relations_from_original_text_database,
        l=self.retrieve_relations_from_existing_link_base,
    """


class TagRelator:
    def __init__(self, link, link_defs, stop_words, edge_file=CONST.file_link, kw_base=None):
        self.better_nodes = self.text = []
        self.selected_edges = self.select_from_better_edges(edge_file)
        self.link_defs = self.search_kw_base(kw_base) if kw_base else (
                link or self.scrap_from_url(link_defs, stop_words))
        self.similar_dict = sd = self.create_equivalence_dict()  # if link else {}
        self.link_defs = {
            node: [sd[tag] if tag in sd else tag for tag in defs]
            for node, defs in self.link_defs.items()}
        self.best_relations = self.find_best_relations()

    def scrap_from_url(self, link_definitions, stop_words):  # CONST.link_defsCONST.stop_words

        from google import google
        # from time import sleep
        num_page = 6
        with open(link_definitions + "n", "w") as defs_file, open(stop_words, "r") as stop_tags:
            stop_words = stop_tags.read().split()
            link_defs = {
                node: no_stops(
                    " ".join(r.description[:-4]for r in google.search(node, num_page)), stop_words)
                for node in self.better_nodes}
            dump(link_defs, defs_file)
        return link_defs

    def select_from_better_edges(self, edge_file):
        with open(edge_file, "r") as edges:
            from json import load
            network = load(edges)
            edges, nodes = network[CONST.edges], network[CONST.nodes]
            self.better_nodes = [
                CONST.node.format(**node)
                for node in nodes if node[CONST.value] >= CONST.node_size_theshold]
            the_nodes = " ".join(self.better_nodes)
            edges = ["{source} {target}".format(**edge).split() for edge in edges
                     if (edge[CONST.source] in the_nodes) and (edge[CONST.target] in the_nodes)]
            return edges

    def create_equivalence_dict(self):
        taglist = sorted(list(set(value for values in self.link_defs.values() for value in values)), reverse=True)
        print("taglist", taglist)
        similarities = {
            word_key: similarity_list for word_key in taglist
            if (similarity_list := list(set(
                similarity for sample_word in taglist
                if (similarity := similars(word_key, sample_word))))) if (len(word_key) > 3)
        }
        entries = sorted(list(similarities.keys()), reverse=True)
        singular_similes = {}
        all_similies = ""
        for entry in entries:
            all_similies += " ".join(sig for sigies in singular_similes.values() for sig in sigies)
            singular_similes.update({entry: similarities[entry]}) if entry not in all_similies else None
        print("singular_similes", singular_similes)
        return {similar: original
                for original, similarity_list in singular_similes.items()
                for similar in similarity_list}

    def find_best_relations(self):
        def match_ocurrences(source, target):
            from collections import Counter
            source_stopper = " ".join(self.similar_dict[source]) if source in self.similar_dict else ""
            target_stopper = " ".join(self.similar_dict[target]) if target in self.similar_dict else ""
            stopper = stop + f"{source} {target}" + source_stopper + target_stopper + f"{source}s {target}s"
            match_count = Counter(
                source_def for source_def in self.link_defs[source] if source_def not in stopper
                for target_def in self.link_defs[target] if target_def not in stopper
                if source_def == target_def)
            counts = [(count, key) for key, count in match_count.items()]
            return sorted(counts, reverse=True)[:6] if counts else None  # "N O N E"

        stop = "profi consumo comum consumo desse prevê poderão contém nestes trabalhos"
        stop += "quais perto admar sisu minas parou veja todos sinop gerên brasileiros dias"
        stop += "instituída através conheça horas aprenderam através informe translati"
        stop += "notar últimas relato segunda acompanhe notar últimas seleção porto capaz entra"
        stop += "confira estará secretário campus mundo vagas reais marx influencing acesso discute"
        stop += "individuo alunos line pesquisados contato pares penna inteira aprendizaje indo"
        stop += "nchez games final maría shiv capra   "
        node_defs = list(self.link_defs.keys())
        return {(source, target): match_ocurrences(source, target)
                for source in node_defs for target in node_defs if source != target}

    def save_best_relations(self, relations_file):  # =CONST.relation_file):
        with open(relations_file, "w") as relation_file:
            relations = [dict(source=source, target=target, label=label[0][1])
                         for (source, target), label in self.best_relations.items() if label]
            dump(relations, relation_file)

    def search_kw_base(self, kw_base, stop_file=CONST.stop_words, ln_defs=CONST.link_defs):
        def search(node_, num_page, text):
            text_len = len(text)
            return [" ".join(text[max(0, index-num_page): min(text_len, index+num_page)])
                    for index, word in enumerate(text) if word == node_]
        with open(kw_base, "r") as kw_file, open(ln_defs, "w") as defs_file, open(stop_file, "r") as stop_tags:
            self.text = kw_file.read().split()[1:]
            stop_words = stop_tags.read().split()
            print(f"stop_words{stop_words}")
            self.text = [tag.lower().strip(".!?;-+(),@#$%&*{}ªº") for tag in self.text]
            self.text = [word for word in self.text if (word not in stop_words) and len(word) > CONST.stop_clipper]
            print("self.text", search('cognição', CONST.num_page, self.text)[:20])
            link_defs = {node: no_stops(" ".join(r for r in search(node, CONST.num_page, self.text)), stop_words)
                         for node in self.better_nodes}
            dump(link_defs, defs_file)
        return link_defs


class Mediator:
    """Runs given predefined scrits to trigger correct execution of TagRelator.

     :param s: word similarity file, each line with  <meaning alias*>.
     :param r: word relation dictionary in json format.
     :param g: graph file to 3D forge viewer in json format.
     :param w: stop words list not relevant to meaning.
     :param kwargs: receive options b or l

     Option action:

         * b: build relations from original text database.
         * l: retrieve relations from existing link base.

     .. seealso::

        Page :ref:`core_relator`

     """

    def __init__(self, s=None, r=None, g=None, w=None, **kwargs):
        self.arguments = dict(
            link_defs=s or CONST.link_defs,
            relation_file=r or CONST.relation_file,
            kw_base=g or CONST.texto_original,
            stop_words=w or CONST.stop_words,
        )
        runner = dict(
            b=self.build_relations_from_original_text_database,
            l=self.retrieve_relations_from_existing_link_base,
        )
        [runner[op](**self.arguments) for op, arg in kwargs.items() if arg]

    @staticmethod
    def build_relations_from_original_text_database(
            link=None, kw_base=CONST.texto_original, link_defs=CONST.link_defs,
            stop_words=CONST.stop_words, relations_file=None, **_
    ):
        tagrel = TagRelator(link,link_defs=link_defs, stop_words=stop_words, kw_base=kw_base)
        print("selected edges", tagrel.selected_edges)
        print("selected nodes", tagrel.better_nodes)
        print("selected nodes defs", tagrel.link_defs)
        print("similar defs", tagrel.similar_dict)
        [print(f"best relations: {relation} -> {label}")
         for relation, label in tagrel.best_relations.items() if label]
        tagrel.save_best_relations(relations_file=relations_file)

    @staticmethod
    def retrieve_relations_from_existing_link_base(
            kw_base=CONST.texto_original, link_defs=CONST.link_defs,
            stop_words=CONST.stop_words, relations_file=CONST.relation_file, **_
    ):
        with open(link_defs, "r") as word_defs:
            from json import load
            links = load(word_defs)
            Mediator.build_relations_from_original_text_database(
                link=links, link_defs='', stop_words=stop_words, kw_base=kw_base, relations_file=relations_file)


if __name__ == "__main__":
    Mediator(**main("indexer.py", option_string=CONST.options))
